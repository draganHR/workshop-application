#!/usr/bin/env node

const fs = require('fs-extra');
const path = require('path');
const ArgumentParser = require('argparse').ArgumentParser;

const nodeModules = 'node_modules';
const binDir = path.join(nodeModules, '.bin');
const shebang = '#!/usr/bin/env bash';
const relativePath = '$(dirname $0)/../..';
const nodeBin = 'node';


const parser = new ArgumentParser({
  'version': '0.0.1',
  'addHelp': true,
  'description': 'Build bin scripts'
});
parser.addArgument(['--all'], {'action': 'storeTrue', 'default': false});
parser.addArgument(['module'], {nargs: '*'});


function buildModule (moduleName) {
  let packageJSON = null;
  let bin = null;
  const modulePath = path.join(moduleName, 'package');
  try {
    packageJSON = require(modulePath);
  } catch (e) {
    console.warn(`Failed to load ${modulePath}`);
    return;
  }
  if (packageJSON.bin === undefined) {
    return;
  } else if (typeof packageJSON.bin === 'string') {
    bin = {};
    bin[moduleName] = packageJSON.bin;
  } else {
    bin = packageJSON.bin;
  }
  Object.keys(bin).forEach((entry) => {
    const binFilename = path.join(binDir, entry);
    const cmd = path.join(nodeModules, moduleName, bin[entry]);
    console.log('Building %s %s...', moduleName, binFilename);
    fs.writeFileSync(binFilename, `${shebang}\n${nodeBin} ${relativePath}/${cmd} "$@"\n`);
    fs.chmodSync(binFilename, '755');
  });
}


function buildAll () {
  fs.removeSync(binDir);
  fs.mkdirSync(binDir);
  fs.readdirSync(nodeModules).forEach((pathName) => {
    if (pathName.indexOf('.') === 0) {
      return;
    }
    try {
      buildModule(pathName);
    } catch (e) {
      console.error(`Error in module ${pathName}\n`, e);
    }
  });
}


if (require.main === module) {
  const args = parser.parseArgs();
  if (args.all) {
    buildAll();
  } else {
    args.module.forEach(buildModule);
  }
}
