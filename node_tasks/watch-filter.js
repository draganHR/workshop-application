/* eslint-disable import/no-extraneous-dependencies */

function WatchFilterPlugin (options) {
  // Setup the plugin instance with options...
}

WatchFilterPlugin.prototype.apply = function apply (compiler) {
  compiler.plugin('done', function done () {

    const wm = require('watchpack/lib/watchpack');
    const oldFunction = wm.prototype.watch;
    const filterPaths = function filterPaths (value) {
      return !value.match(/\/node_modules\//);
    };

    console.log('WatchFilterPlugin patching watchpack/lib/watchpack');

    wm.prototype.watch = function watch (files, directories, startTime) {
      return oldFunction.apply(this, [
        files.filter(filterPaths),
        directories.filter(filterPaths),
        startTime
      ]);
    };
  });
};

module.exports = WatchFilterPlugin;
