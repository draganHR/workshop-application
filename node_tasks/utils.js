require('./polyfill');
const fs = require('fs-extra');
const path = require('path');

const getBundles = function (bundlesDir) {
  const bundles = fs.readdirSync(bundlesDir);
  const pattern = /(.+)\.bundle\.jsx/;
  const result = {};
  bundles.filter(function callback (value) {
    return value.match(pattern) !== null;
  }).forEach(function callback (value) {
    result[value.match(pattern)[1]] = path.resolve(bundlesDir, value);
  });
  return result;
};

module.exports.getBundles = getBundles;
module.exports.assign = Object.assign;
