#!/usr/bin/env node

const fs = require('fs-extra');
const path = require('path');
const ArgumentParser = require('argparse').ArgumentParser;
const sass = require('./sass');


const parser = new ArgumentParser({
  'version': '0.0.1',
  'addHelp': true,
  'description': 'Build SASS'
});
parser.addArgument(['--sourcemap'], {'action': 'storeTrue', 'default': false});

const inputFilename = path.resolve('client/style/main.scss');
const outputPath = 'client_output/style';


if (require.main === module) {
  const args = parser.parseArgs();

  console.log(`Building ${inputFilename} ...`);

  if (!fs.existsSync(outputPath)) {
    fs.mkdirSync(outputPath);
  }
  sass(path.resolve(inputFilename), args.sourcemap).then((result) => {
    fs.writeFile(path.join(outputPath, 'main.css'), result.css);
    fs.writeFile(path.join(outputPath, 'main.css.map'), result.sourcemap);
    console.log(`Building ${inputFilename} done`);
  }).catch((error) => {
    console.error(error);
  });
}
