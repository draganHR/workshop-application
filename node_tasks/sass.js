const sass = require('node-sass');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');
const path = require('path');


function main (file, sourcemap) {
  return new Promise((resolve, reject) => {
    const sassOptions = {
      'file': path.resolve(file),
      'outFile': '/tmp/sass.tmp',
      'sourceComments': true,
      'sourceMap': sourcemap,
      'sourceMapContents': sourcemap,
      'outputStyle': sourcemap ? 'expanded' : 'compressed',
    };
    sass.render(sassOptions, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  })
    .then((sassResult) => {
      const postcssOptions = {
        'from': 'main.scss',
        'to': 'main.css',
        'map': {'inline': false, 'prev': sassResult.map.toString()}
      };
      const postcssPlugins = [
        autoprefixer({'browsers': ['last 2 versions']})
      ];
      return postcss(postcssPlugins)
        .process(sassResult.css.toString('utf-8'), postcssOptions)
        .then((postcssResult) => {
          return {
            'css': postcssResult.css,
            'sourcemap': postcssResult.map.toString()
          };
        });
    });
}

module.exports = main;
