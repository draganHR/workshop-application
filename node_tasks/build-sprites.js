#!/usr/bin/env node

var fs = require("fs-extra");
var path = require("path");
var glob = require("glob");
var templater = require("spritesheet-templates");
var spritesmith = require("spritesmith");
const ArgumentParser = require('argparse').ArgumentParser;


const parser = new ArgumentParser({
  'version': '0.0.1',
  'addHelp': true,
  'description': 'Build sprites'
});
parser.addArgument(['path'], {'nargs': '*'});


function buildSprites (pathName, targetPathName, prefix) {
  const pattern = path.join(pathName, "**/*.png");
  const targetBasePathName = path.basename(targetPathName);
  const sprites = glob.sync(pattern);

  console.log("Building directory %s...", pathName);

  spritesmith.run({src: sprites}, function handleResult (err, result) {
    const templaterSprites = [];
    const templaterData = {
      sprites: templaterSprites,
      spritesheet: {
        width: result.properties.width, height: result.properties.height, image: "../" + targetBasePathName + "/sprites.png"
      }
    };

    if (!fs.existsSync(targetPathName)) {
      fs.mkdirSync(targetPathName);
    }

    Object.keys(result.coordinates).forEach(function (filename) {
      const coordinates = result.coordinates[filename];
      const name = (prefix + "-" + path.basename(filename).replace(/\.[a-z]+$/, "")).replace(/[^\w-]/, "-");
      templaterSprites.push({
        'name': name,
        'x': coordinates.x,
        'y': coordinates.y,
        'width': coordinates.width,
        'height': coordinates.height
      });
    });

// Write files
    fs.writeFileSync(path.join(targetPathName, "sprites.png"), result.image, "binary");
    fs.writeFileSync(path.join(targetPathName, "sprites.scss"), templater(templaterData, {
      'format': 'scss'
    }));
    fs.writeFileSync(path.join(targetPathName, "sprites.css"), templater(templaterData, {
      'format': 'css',
      'formatOpts': {
        'cssSelector': function (sprite) {
          return '.' + sprite.name;
        }
      }
    }));

  });

}

function buildDirectory (pathName) {
  var pattern = path.join(pathName, "*-source");
  glob.sync(pattern).forEach(function (sourcePath) {
    var destPath = sourcePath.replace(/-source$/, "-sprites"),
      prefix = "sprite-" + path.basename(sourcePath).replace(/-source$/, "");
    buildSprites(sourcePath, destPath, prefix);
  });
}

if (require.main === module) {
  const args = parser.parseArgs();
  args.path.forEach(buildDirectory);
}
