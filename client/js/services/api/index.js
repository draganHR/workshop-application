import requests from '../../requests';


class Api {
  constructor (config) {
    const instance = requests(Object.assign({}, config, {
      'xsrfCookieName': 'csrftoken',
      'xsrfHeaderName': 'X-CSRFToken',
    }));

    this.instance = instance.instance;
    this.setHeader = instance.setHeader;
    this.deleteHeader = instance.deleteHeader;
    this.request = instance.request;
    this.get = instance.get;
    this.delete = instance.delete;
    this.head = instance.head;
    this.options = instance.options;
    this.post = instance.post;
    this.put = instance.put;
    this.patch = instance.patch;
  }

  profilePath = '/api/profile/';
  bookingsPath = '/api/bookings/';
  clientsPath = '/api/clients/';
  employeesPath = '/api/employees/';
  vehiclesPath = '/api/vehicles/';
  modelsPath = '/api/models/';

  retrieveProfile = () => this.get(this.profilePath);

  listBookings = () => this.get(`${this.bookingsPath}`);

  createBooking = (data) => this.post(`${this.bookingsPath}`, data);

  retrieveBooking = (id) => this.get(`${this.bookingsPath}${id}/`);

  updateBooking = (id, data) => this.put(`${this.bookingsPath}${id}/`, data);

  deleteBooking = (id) => this.delete(`${this.bookingsPath}${id}/`);

  listClients = () => this.get(`${this.clientsPath}`);

  createClient = (data) => this.post(`${this.clientsPath}`, data);

  retrieveClient = (id) => this.get(`${this.clientsPath}${id}/`);

  updateClient = (id, data) => this.put(`${this.clientsPath}${id}/`, data);

  deleteClient = (id) => this.delete(`${this.clientsPath}${id}/`);

  listEmployees = () => this.get(`${this.employeesPath}`);

  createEmployee = (data) => this.post(`${this.employeesPath}`, data);

  retrieveEmployee = (id) => this.get(`${this.employeesPath}${id}/`);

  updateEmployee = (id, data) => this.put(`${this.employeesPath}${id}/`, data);

  deleteEmployee = (id) => this.delete(`${this.employeesPath}${id}/`);

  listVehicles = () => this.get(`${this.vehiclesPath}`);

  createVehicle = (data) => this.post(`${this.vehiclesPath}`, data);

  retrieveVehicle = (id) => this.get(`${this.vehiclesPath}${id}/`);

  updateVehicle = (id, data) => this.put(`${this.vehiclesPath}${id}/`, data);

  deleteVehicle = (id) => this.delete(`${this.vehiclesPath}${id}/`);

  listModels = () => this.get(`${this.modelsPath}`);
}


const api = new Api();


api.instance.interceptors.response.use((response) => {
  console.log('Api %c Request Success:', 'color: #04B404; font-weight: bold', response);
  return response;
}, (error) => {
  console.log('Api %c Request Error:', 'color: #DF0101; font-weight: bold', error);
  return Promise.reject(error);
});

export default api;
