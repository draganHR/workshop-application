import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import ValidationError from '../../forms/components/ValidationError';
import Widget from '../../forms/components/Widget';


const ClientForm = (props) => {
  console.log('ClientForm props', props);
  const {data, errors, onSubmit, onChange, onCancel} = props;
  return (
    <Form horizontal onSubmit={onSubmit}>
      <ValidationError>{errors['']}</ValidationError>
      <Widget
        name="first_name"
        label="First name"
        error={errors.first_name}
        data={data.first_name}
        onChange={onChange}
      />
      <Widget
        name="last_name"
        label="Last name"
        error={errors.last_name}
        data={data.last_name}
        onChange={onChange}
      />
      <Widget
        name="email_address"
        label="E-mail address"
        error={errors.email_address}
        data={data.email_address}
        onChange={onChange}
      />
      <Widget
        name="phone"
        label="Phone"
        error={errors.phone}
        data={data.phone}
        onChange={onChange}
      />
      <Widget
        name="address_line_1"
        label="Address Line 1"
        error={errors.address_line_1}
        data={data.address_line_1}
        onChange={onChange}
      />
      <Widget
        name="address_line_2"
        label="Address Line 2"
        error={errors.address_line_2}
        data={data.address_line_2}
        onChange={onChange}
      />
      <Widget
        name="address_line_3"
        label="Address Line 3"
        error={errors.address_line_3}
        data={data.address_line_3}
        onChange={onChange}
      />
      <Widget
        name="city"
        label="City"
        error={errors.city}
        data={data.city}
        onChange={onChange}
      />
      <Widget
        name="postal"
        label="Postal"
        error={errors.postal}
        data={data.postal}
        onChange={onChange}
      />
      <Widget
        name="country"
        label="Country"
        error={errors.country}
        data={data.country}
        onChange={onChange}
      />
      <Widget
        name="notes"
        label="Notes"
        componentClass="textarea"
        rows="6"
        error={errors.notes}
        data={data.notes}
        onChange={onChange}
      />
      <FormGroup>
        <Col smOffset={3} sm={9}>
          <ButtonToolbar>
            <Button onClick={onCancel}>Cancel</Button>
            <Button type="submit" bsStyle="primary">
              <Glyphicon glyph="save" />{' '}
              Submit
            </Button>
          </ButtonToolbar>
        </Col>
      </FormGroup>
    </Form>
  );
};

ClientForm.propTypes = {
  'data': PropTypes.shape({
    'first_name': PropTypes.string,
    'last_name': PropTypes.string,
    'email_address': PropTypes.string,
    'phone': PropTypes.string,
    'address_line_1': PropTypes.string,
    'address_line_2': PropTypes.string,
    'address_line_3': PropTypes.string,
    'city': PropTypes.string,
    'postal': PropTypes.string,
    'country': PropTypes.string,
    'notes': PropTypes.string,
  }).isRequired,
  'errors': PropTypes.shape({}),
  'onSubmit': PropTypes.func.isRequired,
  'onCancel': PropTypes.func.isRequired,
  'onChange': PropTypes.func.isRequired
};

export default ClientForm;
