import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import ClientsList from '../components/ClientsList';
import * as actions from '../redux';
import ClientEditor from './ClientEditor';


class Clients extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'activePage': 1,
      'showDialog': false,
    };
  }

  componentDidMount () {
    this.loadData();
  }

  loadData () {
    this.props.actions.getClients();
  }

  handleNew = () => {
    this.setState({
      'activeId': null,
      'showDialog': true
    });
  };

  handleEdit = (item) => {
    this.setState({
      'activeId': item.id,
      'showDialog': true
    });
  };

  handleDelete = (item) => {
    this.props.actions.deleteClient(item.id).then(
      () => this.props.actions.getClients()
    );
  };

  handleCloseDialog = () => {
    this.setState({
      'activeId': null,
      'showDialog': false
    });
    this.loadData();
  };

  render () {
    const {showDialog, activeId} = this.state;
    return (
      <div>
        { showDialog &&
        <ClientEditor
          id={activeId}
          onHide={this.handleCloseDialog}
          onClose={this.handleCloseDialog}
        />
        }
        <ButtonToolbar>
          <Button onClick={this.handleNew} bsStyle="primary">New Client</Button>
        </ButtonToolbar>
        <ClientsList
          results={this.props.results}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
      </div>
    );
  }
}


Clients.propTypes = {
  'results': PropTypes.arrayOf(PropTypes.shape()).isRequired,
  'actions': PropTypes.shape().isRequired,
};


const mapState = (state) => ({
  'results': state.clients.results, //
});


const mapDispatch = (dispatch) => ({
  'actions': bindActionCreators({
    'getClients': actions.getClients,
    'deleteClient': actions.deleteClient,
  }, dispatch)
});

export default connect(mapState, mapDispatch)(Clients);
