import {createReducer} from 'reduxsauce';
import {asyncActionTypes, asyncActionFactory} from '../async';
import api from '../services/api';


export const GET_CLIENTS = asyncActionTypes('GET_CLIENTS');
export const DELETE_CLIENT = asyncActionTypes('DELETE_CLIENT');


export const getClients = asyncActionFactory(GET_CLIENTS.BASE, (dispatch, getState) => () => {
  console.log('callback; state: ', getState());
  return api.listClients();
}, {});


export const deleteClient = asyncActionFactory(DELETE_CLIENT.BASE, () => (id) => {
  console.log('deleteClient id:', id);
  return api.deleteClient(id);
}, {});


const initialState = {
  'isPending': false,
  'results': [],
  'count': 0,
  'previous': null,
  'next': null,
  'error': null,
};


const handlers = {
  [GET_CLIENTS.PENDING]: (state) => ({
    ...state,
    'isPending': true,
    'error': null,
  }),
  [GET_CLIENTS.SUCCESS]: (state, action) => ({
    ...state,
    'isPending': false,
    'error': null,
    'results': action.response.data.results,
    'count': action.response.data.count,
    'previous': action.response.data.previous,
    'next': action.response.data.next,
  }),
  [GET_CLIENTS.ERROR]: (state) => ({
    ...state,
    'isPending': false,
    'error': 'YES',
  }),
};


const reducer = createReducer(initialState, handlers);
export default reducer;

/*
function getProfileData (state = {
  'isFetching': false,
  'data': {}
}, action) {
  switch (action.type) {
    case `${FETCH_PROFILE_DATA}_REQUEST`:
      return Object.assign({}, state, {
        'isFetching': true
      });
    case `${FETCH_PROFILE_DATA}_SUCCESS`:
      return Object.assign({}, state, {
        'isFetching': false,
      });
    case `${FETCH_PROFILE_DATA}_FAILURE`:
      return Object.assign({}, state, {
        'isFetching': false,
      });
    default:
      return state;
  }
}*/

/*
export const postProfileData = typeToReducer({
  [actions.SAVE_PROFILE]: {
    [PENDING]: (state) => Object.assign({}, state, {
      'isFetching': true,
      'error': null,
    }),
    [FULFILLED]: (state, action) => Object.assign({}, state, {
      'isFetching': false,
      'data': action.payload.data,
      'error': null,
    }),
    [REJECTED]: (state, action) => Object.assign({}, state, {
      'isFetching': false,
      'error': action.error,
    }),
  }
}, {
  'isFetching': false,
  'data': null,
  'error': null,
});
*/

/*
function postProfileData (state = {
  'isPosting': false
}, action) {
  switch (action.type) {
    case 'SAVE_PROFILE_DATA_REQUEST':
      return Object.assign({}, state, {
        'isPosting': true
      });
    case 'SAVE_PROFILE_DATA_SUCCESS':
      return Object.assign({}, state, {
        'isPosting': false
      });
    case 'SAVE_PROFILE_DATA_FAILURE':
      // TODO: check type of error
      return Object.assign({}, state, {
        'isPosting': false
      });
    default:
      return state;
  }
}*/

/*

export default combineReducers({
  'get': getProfileData,
  'post': postProfileData,
});

*/
