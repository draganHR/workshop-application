/**
 * Redux async actions factory
 *
 * License: MIT
 */

export const asyncActionTypes = (type) => ({
  'BASE': `${type}`,
  'PENDING': `${type}_PENDING`,
  'SUCCESS': `${type}_SUCCESS`,
  'ERROR': `${type}_ERROR`,
});


export function asyncActionFactory (type, callback, payload) {
  return (...args) => {
    if (
      typeof type !== 'string'
    ) {
      throw new Error('Expected string type.');
    }

    if (typeof callback !== 'function') {
      throw new Error('Expected callback to be a function.');
    }

    const types = asyncActionTypes(type);

    return (dispatch, getState) => {
      dispatch(Object.assign({}, payload, {
        'type': types.PENDING
      }));

      return callback(dispatch, getState)(...args).then(
        (response) => dispatch(Object.assign({}, payload, {
          response,
          'type': types.SUCCESS
        })))
        .catch(
          (error) => {
            dispatch(Object.assign({}, payload, {
              error,
              'type': types.ERROR
            }));
            throw error;
          });
    };
  };
}
