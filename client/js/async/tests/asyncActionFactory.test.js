import {asyncActionFactory, asyncActionTypes} from '../index';
import mockStore from '../../app/tests/mockStore';

const {describe, it, expect} = global;


describe('asyncActionFactory', () => {
  it('call returns promise', () => {
    const store = mockStore({});
    const FOO = asyncActionTypes('FOO');
    const foo = asyncActionFactory(FOO.BASE, (dispatch) => () => Promise.resolve('yay success'), {});

    const promise = store.dispatch(foo());
    expect(promise).toBeInstanceOf(Promise);
  });

  it('call dispatches PENDING', () => {
    const store = mockStore({});
    const FOO = asyncActionTypes('FOO');
    const foo = asyncActionFactory(FOO.BASE, (dispatch) => () => Promise.resolve(`yay success`), {});

    store.dispatch(foo());
    const actions = store.getActions();
    expect(actions).toEqual([{'type': FOO.PENDING}]);
  });

  it('resolve dispatches SUCCESS', () => {
    expect.assertions(2);

    const store = mockStore({});
    const FOO = asyncActionTypes('FOO');
    const foo = asyncActionFactory(FOO.BASE, (dispatch, getState) => (p) => Promise.resolve(`yay success: ${p}`), {});

    const param = 'hello';
    const promise = store.dispatch(foo(param));
    return promise.then((data) => {
      const expectedPayload = {
        'type': FOO.SUCCESS,
        'response': `yay success: ${param}`,
      };
      const actions = store.getActions();

      expect(data).toEqual(expectedPayload);
      expect(actions).toEqual([
        {'type': FOO.PENDING},
        expectedPayload]);
    });
  });

  it('reject dispatches ERROR', () => {
    expect.assertions(2);

    const store = mockStore({});
    const FOO = asyncActionTypes('FOO');
    const foo = asyncActionFactory(FOO.BASE, (dispatch, getState) => (p) => Promise.reject(new Error(`uh-oh fail: ${p}`)), {});

    const param = 'hello';
    const promise = store.dispatch(foo(param));
    return promise.catch((error) => {
      const actions = store.getActions();

      expect(error.message).toEqual(`uh-oh fail: ${param}`);
      expect(actions).toEqual([
        {'type': FOO.PENDING},
        {'type': FOO.ERROR, 'error': error}]);
    });
  });

  it('resolve dispatches SUCCESS (async example)', async () => {
    expect.assertions(2);

    const store = mockStore({});
    const FOO = asyncActionTypes('FOO');
    const foo = asyncActionFactory(FOO.BASE, (dispatch, getState) => (p) => Promise.resolve(`yay success: ${p}`), {});

    const param = 'hello';
    const data = await store.dispatch(foo(param));

    const expectedPayload = {
      'type': FOO.SUCCESS,
      'response': `yay success: ${param}`,
    };
    const actions = store.getActions();

    expect(data).toEqual(expectedPayload);
    expect(actions).toEqual([
      {'type': FOO.PENDING},
      expectedPayload]);
  });

  it('reject dispatches ERROR (async example)', async () => {
    expect.assertions(2);

    const store = mockStore({});
    const FOO = asyncActionTypes('FOO');
    const foo = asyncActionFactory(FOO.BASE, (dispatch, getState) => (p) => Promise.reject(new Error(`uh-oh fail: ${p}`)), {});

    const param = 'hello';
    try {
      await store.dispatch(foo(param));
    } catch (error) {
      const actions = store.getActions();

      expect(error.message).toEqual(`uh-oh fail: ${param}`);
      expect(actions).toEqual([
        {'type': FOO.PENDING},
        {'type': FOO.ERROR, 'error': error}]);
    }
  });
});
