import axiosDefaults from 'axios/lib/defaults';
import axios from 'axios/lib/axios';


axiosDefaults.xsrfCookieName = 'csrftoken';
axiosDefaults.xsrfHeaderName = 'X-CSRFToken';


// axios-response-logger
axios.interceptors.response.use(
  (res) => {
    console.log('%c Request Success:', 'color: #04B404; font-weight: bold', res);
    return res;
  },
  (err) => {
    console.log('%c Request Error:', 'color: #DF0101; font-weight: bold', err);
    return Promise.reject(err);
  }
);
