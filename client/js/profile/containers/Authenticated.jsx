import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../redux';


class Authenticated extends React.Component {
  getChildContext () {
    return {
      'profile': {
        'username': this.props.username,
        'email': this.props.email,
      }
    };
  }

  componentDidMount () {
    if (this.props.username === null) {
      // Retrieve profile data
      this.props.actions.getProfile();
    }
  }

  render () {
    return this.props.username ? this.props.children : null;
  }
}


Authenticated.propTypes = {
  'username': PropTypes.string,
  'email': PropTypes.string,
  'children': PropTypes.node,
  'actions': PropTypes.objectOf(PropTypes.func),
};


Authenticated.defaultProps = {
};


Authenticated.childContextTypes = {
  'profile': PropTypes.shape({
    'username': PropTypes.string,
    'email': PropTypes.string,
  }),
};


const mapState = (state) => ({
  'username': state.profile.username,
  'email': state.profile.email,
});


const mapDispatch = (dispatch) => ({
  'actions': bindActionCreators({
    'getProfile': actions.getProfile,
  }, dispatch)
});

export default connect(mapState, mapDispatch)(Authenticated);
