import {createReducer} from 'reduxsauce';
import {asyncActionTypes, asyncActionFactory} from '../async';
import api from '../services/api';


export const GET_PROFILE = asyncActionTypes('GET_PROFILE');


export const getProfile = asyncActionFactory(GET_PROFILE.BASE, () => () => {
  console.log('getProfile');
  return api.retrieveProfile();
}, {});


const initialState = {
  'isPending': false,
  'username': null,
  'email': null,
  'error': null,
};


const handlers = {
  [GET_PROFILE.PENDING]: (state) => ({
    ...state,
    'isPending': true,
    'error': null,
  }),
  [GET_PROFILE.SUCCESS]: (state, action) => ({
    ...state,
    'isPending': false,
    'error': null,
    'username': action.response.data.username,
    'email': action.response.data.email,
  }),
  [GET_PROFILE.ERROR]: (state) => ({
    ...state,
    'isPending': false,
    'error': 'YES',
  }),
};


const reducer = createReducer(initialState, handlers);
export default reducer;
