import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import BookingsList from '../components/BookingsList';
import * as actions from '../redux';
import BookingEditor from './BookingEditor';


class Bookings extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'activePage': 1,
      'showDialog': false,
    };
  }

  componentDidMount () {
    this.loadData();
  }

  loadData () {
    this.props.actions.getBookings();
  }

  handleNew = () => {
    this.setState({
      'activeId': null,
      'showDialog': true
    });
  };

  handleEdit = (item) => {
    this.setState({
      'activeId': item.id,
      'showDialog': true
    });
  };

  handleDelete = (item) => {
    this.props.actions.deleteBooking(item.id).then(
      () => this.props.actions.getBookings()
    );
  };

  handleCloseDialog = () => {
    this.setState({
      'activeId': null,
      'showDialog': false
    });
    this.loadData();
  };

  render () {
    const {showDialog, activeId} = this.state;
    return (
      <div>
        { showDialog &&
        <BookingEditor
          id={activeId}
          onHide={this.handleCloseDialog}
          onClose={this.handleCloseDialog}
        />
        }
        <ButtonToolbar>
          <Button onClick={this.handleNew} bsStyle="primary">New Booking</Button>
        </ButtonToolbar>
        <BookingsList
          results={this.props.results}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
      </div>
    );
  }
}


Bookings.propTypes = {
  'results': PropTypes.arrayOf(PropTypes.shape()).isRequired,
  'actions': PropTypes.shape().isRequired,
};


const mapState = (state) => ({
  'results': state.bookings.results,
});


const mapDispatch = (dispatch) => ({
  'actions': bindActionCreators({
    'getBookings': actions.getBookings,
    'deleteBooking': actions.deleteBooking,
  }, dispatch)
});

export default connect(mapState, mapDispatch)(Bookings);
