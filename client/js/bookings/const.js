
export const STATUS = {
  '0': 'New',
  '1': 'Pending',
  '2': 'Working',
  '3': 'Finished',
  '4': 'Closed',
};

export const PRIORITY = {
  '0': 'Low',
  '1': 'Medium',
  '2': 'High',
};
