/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import ListGroup from 'react-bootstrap/lib/ListGroup';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';


export default class BookingsList extends React.Component {
  render () {
    const {results} = this.props;

    return (
      <section>
        <ListGroup>
          {results.map((item) => (
            <ListGroupItem
              key={item.id}
              header={`#${item.id} - ${item.priority} - ${item.status} ${item.created_at}`}
            >
              <span className="btn-group">
                <Button onClick={() => this.props.onEdit(item)} title="Edit"><Glyphicon glyph="edit" /></Button>
                <Button onClick={() => this.props.onDelete(item)} title="Delete"><Glyphicon glyph="trash" /></Button>
              </span>
              Vehicle: {item.vehicle} Employee: {item.employee}
            </ListGroupItem>
          ))}
        </ListGroup>
      </section>
    );
  }
}


BookingsList.propTypes = {
  'results': PropTypes.arrayOf(PropTypes.shape()).isRequired,
  'onEdit': PropTypes.func,
  'onDelete': PropTypes.func,
};


BookingsList.defaultProps = {
  'onEdit': () => {},
  'onDelete': () => {},
};
