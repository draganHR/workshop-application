import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import AsyncSelectOptions from '../../forms/containers/AsyncSelectOptions';
import ValidationError from '../../forms/components/ValidationError';
import Widget from '../../forms/components/Widget';
import {PRIORITY, STATUS} from '../const';
import {getVehiclesChoices} from '../../vehicles/utils';
import {getEmployeesChoices} from '../../employees/utils';


const statusChoices = Object.keys(STATUS).map((key) =>
  <option value={key} key={key}>{ STATUS[key] }</option>
);


const priorityChoices = Object.keys(PRIORITY).map((key) =>
  <option value={key} key={key}>{ PRIORITY[key] }</option>
);


const BookingForm = (props) => {
  console.log('BookingForm props', props);
  const {data, errors, onSubmit, onChange, onCancel} = props;
  return (
    <Form horizontal onSubmit={onSubmit}>
      <ValidationError>{errors['']}</ValidationError>
      <Widget
        name="vehicle"
        label="Vehicle"
        componentClass="select"
        error={errors.vehicle}
        data={data.vehicle}
        onChange={onChange}
      >
        <AsyncSelectOptions getOptions={getVehiclesChoices} defaultValue={data.vehicle} />
      </Widget>
      <Widget
        name="employee"
        label="Employee"
        componentClass="select"
        error={errors.employee}
        data={data.employee}
        onChange={onChange}
      >
        <AsyncSelectOptions getOptions={getEmployeesChoices} defaultValue={data.employee} />
      </Widget>
      <Widget
        name="status"
        label="Status"
        componentClass="select"
        error={errors.status}
        data={data.status}
        onChange={onChange}
      >
        {statusChoices}
      </Widget>
      <Widget
        name="priority"
        label="Priority"
        componentClass="select"
        error={errors.priority}
        data={data.priority}
        onChange={onChange}
      >
        {priorityChoices}
      </Widget>
      <Widget
        name="billed_at"
        label="Billed at"
        type="datetime-local"
        error={errors.billed_at}
        data={data.billed_at}
        onChange={onChange}
      />
      <Widget
        name="hours_spent"
        label="Hours spent"
        type="number"
        error={errors.hours_spent}
        data={data.hours_spent}
        onChange={onChange}
      />
      <Widget
        name="notes"
        label="Notes"
        componentClass="textarea"
        rows="6"
        error={errors.notes}
        data={data.notes}
        onChange={onChange}
      />
      <FormGroup>
        <Col smOffset={3} sm={9}>
          <ButtonToolbar>
            <Button onClick={onCancel}>Cancel</Button>
            <Button type="submit" bsStyle="primary">
              <Glyphicon glyph="save" />{' '}
              Submit
            </Button>
          </ButtonToolbar>
        </Col>
      </FormGroup>
    </Form>
  );
};

BookingForm.propTypes = {
  'data': PropTypes.shape({
    'vehicle': PropTypes.number,
    'employee': PropTypes.number,
    'status': PropTypes.number,
    'priority': PropTypes.number,
    'hours_spent': PropTypes.number,
    'notes': PropTypes.string,
  }).isRequired,
  'errors': PropTypes.shape({}),
  'onSubmit': PropTypes.func.isRequired,
  'onCancel': PropTypes.func.isRequired,
  'onChange': PropTypes.func.isRequired
};

export default BookingForm;
