import {createReducer} from 'reduxsauce';
import {asyncActionTypes, asyncActionFactory} from '../async';
import api from '../services/api';


export const GET_BOOKINGS = asyncActionTypes('GET_BOOKINGS');
export const DELETE_BOOKING = asyncActionTypes('DELETE_BOOKING');


export const getBookings = asyncActionFactory(GET_BOOKINGS.BASE, () => () => {
  console.log('getBookings');
  return api.listBookings();
}, {});


export const deleteBooking = asyncActionFactory(DELETE_BOOKING.BASE, () => (id) => {
  console.log('deleteBooking id:', id);
  return api.deleteBooking(id);
}, {});


const initialState = {
  'isPending': false,
  'results': [],
  'count': 0,
  'previous': null,
  'next': null,
  'error': null,
};


const handlers = {
  [GET_BOOKINGS.PENDING]: (state) => ({
    ...state,
    'isPending': true,
    'error': null,
  }),
  [GET_BOOKINGS.SUCCESS]: (state, action) => ({
    ...state,
    'isPending': false,
    'error': null,
    'results': action.response.data.results,
    'count': action.response.data.count,
    'previous': action.response.data.previous,
    'next': action.response.data.next,
  }),
  [GET_BOOKINGS.ERROR]: (state) => ({
    ...state,
    'isPending': false,
    'error': 'YES',
  }),
};


const reducer = createReducer(initialState, handlers);
export default reducer;
