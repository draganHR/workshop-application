/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';


class AsyncSelectOptions extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'options': {
        [props.defaultValue]: '',
      }
    };
  }

  componentDidMount () {
    this.props.getOptions().then((options) => {
      this.setState({'options': options});
    });
  }

  render () {
    const {defaultOption} = this.props;
    const {options} = this.state;
    return [
      defaultOption,
      ...Object.keys(options).map((key) =>
        <option value={key} key={key}>{ options[key] }</option>
      )
    ];
  }
}

AsyncSelectOptions.propTypes = {
  'defaultValue': PropTypes.any,
  'defaultOption': PropTypes.any,
  'getOptions': PropTypes.func.isRequired,
};


AsyncSelectOptions.defaultProps = {
  'defaultValue': null,
  'defaultOption': <option value={''} key={''}>{' '}</option>,
};

export default AsyncSelectOptions;
