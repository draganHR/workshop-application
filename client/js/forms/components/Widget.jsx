import React from 'react';
import PropTypes from 'prop-types';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';
import Col from 'react-bootstrap/lib/Col';
import ValidationError from './ValidationError';


const Widget = function Widget (props) {
  const {label, help, error, defaultValue, data, ...extra} = props;
  return (
    <FormGroup validationState={error ? 'error' : null}>
      <Col componentClass={ControlLabel} sm={3}>
        {label}:
      </Col>
      <Col sm={9}>
        <FormControl
          defaultValue={data === undefined ? defaultValue : data}
          {...extra}
        />
        <ValidationError>{error}</ValidationError>
        { help && <HelpBlock>{help}</HelpBlock>}
      </Col>
    </FormGroup>
  );
};


Widget.propTypes = {
  'componentClass': FormControl.propTypes.componentClass,
  'type': FormControl.propTypes.type,
  'name': PropTypes.string.isRequired,
  'label': PropTypes.string.isRequired,
  'defaultValue': PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number,
    PropTypes.string,
  ]),
  'colCtrlLbl': PropTypes.number,
  'colCtrl': PropTypes.number,
  'placeholder': PropTypes.string,
  'help': PropTypes.string,
  'error': ValidationError.propTypes.children,
  'onChange': PropTypes.func.isRequired,
};

Widget.propTypes.data = Widget.propTypes.defaultValue; // alias


Widget.defaultProps = {
  'componentClass': 'input',
  'type': '',
};


export default Widget;
