import React from 'react';
import PropTypes from 'prop-types';
import {bsClass, getClassSet, splitBsProps} from 'react-bootstrap/lib/utils/bootstrapUtils';
import classNames from 'classnames';


const ValidationError = (props) => {
  const {className, children, ...other} = props;
  const [bsProps, elementProps] = splitBsProps(other);
  const classes = getClassSet(bsProps);

  let items;
  if (children === null) {
    items = [];
  } else if (Array.isArray(children)) {
    items = children;
  } else {
    items = [children];
  }

  if (items.length === 0) {
    return null;
  }

  return (
    <label
      {...elementProps}
      className={classNames(className, classes)}
    >
      {items.map((item, index) =>
        <span key={index}>{item}</span>
      )}
    </label>
  );
};


ValidationError.propTypes = {
  'className': PropTypes.string,
  'children': PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
};


ValidationError.defaultProps = {
  'children': null,
};


export default bsClass('validation-error', ValidationError);
