import api from '../services/api';


export const getModelChoices = () => api.listModels().then((response) => {
  const results = response.data.results;
  const choices = {};
  results.forEach((element) => {
    choices[element.id] = element.name;
  });
  return choices;
}, (error) => {
  console.log('Choices request error:', error);
});


export const getVehiclesChoices = () => api.listVehicles().then((response) => {
  const results = response.data.results;
  const choices = {};
  results.forEach((element) => {
    choices[element.id] = element.license_number;
  });
  return choices;
}, (error) => {
  console.log('Choices request error:', error);
});
