import {createReducer} from 'reduxsauce';
import {asyncActionTypes, asyncActionFactory} from '../async';
import api from '../services/api';


export const GET_VEHICLES = asyncActionTypes('GET_VEHICLES');
export const DELETE_VEHICLE = asyncActionTypes('DELETE_VEHICLE');


export const getVehicles = asyncActionFactory(GET_VEHICLES.BASE, (dispatch, getState) => () => {
  console.log('callback; state: ', getState());
  return api.listVehicles();
}, {});


export const deleteVehicle = asyncActionFactory(DELETE_VEHICLE.BASE, () => (id) => {
  console.log('deleteVehicle id:', id);
  return api.deleteVehicle(id);
}, {});


const initialState = {
  'isPending': false,
  'results': [],
  'count': 0,
  'previous': null,
  'next': null,
  'error': null,
};


const handlers = {
  [GET_VEHICLES.PENDING]: (state) => ({
    ...state,
    'isPending': true,
    'error': null,
  }),
  [GET_VEHICLES.SUCCESS]: (state, action) => ({
    ...state,
    'isPending': false,
    'error': null,
    'results': action.response.data.results,
    'count': action.response.data.count,
    'previous': action.response.data.previous,
    'next': action.response.data.next,
  }),
  [GET_VEHICLES.ERROR]: (state) => ({
    ...state,
    'isPending': false,
    'error': 'YES',
  }),
};


const reducer = createReducer(initialState, handlers);
export default reducer;
