import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import AsyncSelectOptions from '../../forms/containers/AsyncSelectOptions';
import ValidationError from '../../forms/components/ValidationError';
import Widget from '../../forms/components/Widget';
import {getClientChoices} from '../../clients/utils';
import {getModelChoices} from '../../vehicles/utils';


const VehicleForm = (props) => {
  console.log('VehicleForm props', props);
  const {data, errors, onSubmit, onChange, onCancel} = props;
  return (
    <Form horizontal onSubmit={onSubmit}>
      <ValidationError>{errors['']}</ValidationError>
      <Widget
        name="client"
        label="Client"
        type="number"
        componentClass="select"
        error={errors.client}
        data={data.client}
        onChange={onChange}
      >
        <AsyncSelectOptions getOptions={getClientChoices} defaultValue={data.client} />
      </Widget>
      <Widget
        name="model"
        label="Model"
        type="number"
        componentClass="select"
        error={errors.model}
        data={data.model}
        onChange={onChange}
      >
        <AsyncSelectOptions getOptions={getModelChoices} defaultValue={data.model} />
      </Widget>
      <Widget
        name="year"
        label="Year"
        type="number"
        error={errors.year}
        data={data.year}
        onChange={onChange}
      />
      <Widget
        name="license_number"
        label="License number"
        error={errors.license_number}
        data={data.license_number}
        onChange={onChange}
      />
      <Widget
        name="current_mileage"
        label="Current mileage"
        type="number"
        error={errors.current_mileage}
        data={data.current_mileage}
        onChange={onChange}
      />
      <Widget
        name="engine_size"
        label="Engine size"
        error={errors.engine_size}
        data={data.engine_size}
        onChange={onChange}
      />
      <Widget
        name="notes"
        label="Notes"
        componentClass="textarea"
        rows="6"
        error={errors.notes}
        data={data.notes}
        onChange={onChange}
      />
      <FormGroup>
        <Col smOffset={3} sm={9}>
          <ButtonToolbar>
            <Button onClick={onCancel}>Cancel</Button>
            <Button type="submit" bsStyle="primary">
              <Glyphicon glyph="save" />{' '}
              Submit
            </Button>
          </ButtonToolbar>
        </Col>
      </FormGroup>
    </Form>
  );
};

VehicleForm.propTypes = {
  'data': PropTypes.shape({
    'client': PropTypes.number,
    'model': PropTypes.number,
    'year': PropTypes.number,
    'license_number': PropTypes.string,
    'current_mileage': PropTypes.number,
    'engine_size': PropTypes.string,
    'notes': PropTypes.string,
  }).isRequired,
  'errors': PropTypes.shape({}),
  'onSubmit': PropTypes.func.isRequired,
  'onCancel': PropTypes.func.isRequired,
  'onChange': PropTypes.func.isRequired
};

export default VehicleForm;
