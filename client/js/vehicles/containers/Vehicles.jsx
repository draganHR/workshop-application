import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import VehiclesList from '../components/VehiclesList';
import * as actions from '../redux';
import VehicleEditor from './VehicleEditor';


class Vehicles extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'activePage': 1,
      'showDialog': false,
    };
  }

  componentDidMount () {
    this.loadData();
  }

  loadData () {
    this.props.actions.getVehicles();
  }

  handleNew = () => {
    this.setState({
      'activeId': null,
      'showDialog': true
    });
  };

  handleEdit = (item) => {
    this.setState({
      'activeId': item.id,
      'showDialog': true
    });
  };

  handleDelete = (item) => {
    this.props.actions.deleteVehicle(item.id).then(
      () => this.props.actions.getVehicles()
    );
  };

  handleCloseDialog = () => {
    this.setState({
      'activeId': null,
      'showDialog': false
    });
    this.loadData();
  };

  render () {
    const {showDialog, activeId} = this.state;
    return (
      <div>
        { showDialog &&
        <VehicleEditor
          id={activeId}
          onHide={this.handleCloseDialog}
          onClose={this.handleCloseDialog}
        />
        }
        <ButtonToolbar>
          <Button onClick={this.handleNew} bsStyle="primary">New Vehicle</Button>
        </ButtonToolbar>
        <VehiclesList
          results={this.props.results}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
      </div>
    );
  }
}


Vehicles.propTypes = {
  'results': PropTypes.arrayOf(PropTypes.shape()).isRequired,
  'actions': PropTypes.shape().isRequired,
};


const mapState = (state) => ({
  'results': state.vehicles.results,
});


const mapDispatch = (dispatch) => ({
  'actions': bindActionCreators({
    'getVehicles': actions.getVehicles,
    'deleteVehicle': actions.deleteVehicle,
  }, dispatch)
});

export default connect(mapState, mapDispatch)(Vehicles);
