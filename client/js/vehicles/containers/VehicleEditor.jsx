import React from 'react';
import PropTypes from 'prop-types';
import Panel from 'react-bootstrap/lib/Panel';
import VehicleForm from '../components/VehicleForm';
import api from '../../services/api';
import ModalWrapper from '../../ui/components/ModalWrapper';


const defaultValues = {
};


class VehicleEditor extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'initial': null, // initial values
      'values': {},
      'errors': {},
      'isLoading': false,
    };
  }

  componentDidMount () {
    this.handleInitial(this.props.id);
  }

  setInitial (values) {
    this.setState({
      'initial': values,
      'values': values,
      'errors': {},
      'isLoading': false,
    });
  }

  handleInitial = (id) => {
    if (!id) {
      this.setInitial(defaultValues);
      return;
    }
    this.setState({
      'errors': {},
      'isLoading': true,
    });
    api.retrieveVehicle(id)
      .then((response) => {
        console.log('Initial:', response);
        this.setInitial(response.data);
      }, (error) => {
        console.error('Initial error:', error);
        this.setState({
          'isLoading': false,
        });
      })
    ;
  };

  handleChange = (e) => {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    const newValues = Object.assign({}, this.state.values, {[e.target.name]: value});
    this.setState({'values': newValues});
  };

  handleSubmit = (e) => {
    e.preventDefault();
    console.log('handleSubmit', this.props, this.state);

    if (this.state.isLoading) {
      return;
    }
    this.setState({
      'errors': {},
      'isLoading': true,
    });

    const values = {...this.state.values};

    let promise;
    if (this.props.id) {
      promise = api.updateVehicle(this.props.id, values);
    } else {
      promise = api.createVehicle(values);
    }

    promise.then((response) => {
      console.log('Submit:', response);
      this.setState({
        'errors': {},
        'isLoading': false,
      });
      this.handleClose();
    }, (error) => {
      let errors = null;
      try {
        errors = error.data.detail.fields || null;
      } catch (err) {
        console.warn('No input errors found? Message: ', err.message);
      }
      if (errors === null) {
        errors = {'': `Unexpected error (${error.status}). Please try again.`};
      }
      console.log('Submit error errors:', errors);
      this.setState({
        'errors': errors,
        'isLoading': false,
      });
    });
  };

  handleClose = () => {
    this.props.onClose();
  };

  render () {
    const {initial, errors} = this.state;
    return (
      <ModalWrapper
        show
        title="Vehicle form"
        onHide={this.handleClose}
      >
        <Panel>
          {initial &&
            <VehicleForm
              data={initial}
              errors={errors}
              onSubmit={this.handleSubmit}
              onChange={this.handleChange}
              onCancel={this.handleClose}
            />
          }
        </Panel>
      </ModalWrapper>
    );
  }
}


VehicleEditor.propTypes = {
  'id': PropTypes.number,
  'onClose': PropTypes.func,
};


VehicleEditor.defaultProps = {
  'id': null,
  'onClose': () => {},
};

export default VehicleEditor;
