import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import Header from './ui/components/Header';
import Body from './ui/containers/Body';
import config from './config';
import {route} from './shim/jsbridge';
import store from './app/store';
import Authenticated from './profile/containers/Authenticated';

console.warn('config:', config);

// jsbridge
console.log('jsbridge.route index:', route('index'));

const rootElement = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <Authenticated>
      <div>
        <Header />
        <Body />
      </div>
    </Authenticated>
  </Provider>,
  rootElement);
