import React from 'react';
import PropTypes from 'prop-types';
import PageHeader from 'react-bootstrap/lib/PageHeader';


class Header extends React.Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  render () {
    const profile = this.context.profile || {};
    return (
      <PageHeader>
        MyApp <small>Lorem ipsum dolor sit amet</small>
        <small className="pull-right">{profile.username} | {profile.email} | <a href="/accounts/logout/">Logout</a></small>
      </PageHeader>
    );
  }
}

Header.contextTypes = {
  'profile': PropTypes.shape({}),
};

export default Header;
