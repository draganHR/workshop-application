import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-bootstrap/lib/Modal';


const ModalWrapper = ({children, show, title, onHide}) => (
  <Modal show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
      {children}
    </Modal.Body>
  </Modal>
);


ModalWrapper.propTypes = {
  'title': PropTypes.string,
  'show': PropTypes.bool,
  'children': PropTypes.node,
  'onHide': PropTypes.func,
};


ModalWrapper.defaultProps = {
  'show': true,
  'onHide': () => {},
};


export default ModalWrapper;
