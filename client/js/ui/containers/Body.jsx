import React from 'react';
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';

import Bookings from '../../bookings/containers/Bookings';
import Clients from '../../clients/containers/Clients';
import Vehicles from '../../vehicles/containers/Vehicles';
import Employees from '../../employees/containers/Employees';


export default class Body extends React.Component {
  constructor (props) {
    super(props);
    this.state = {'key': 'bookings'};
  }

  handleSelect = (key) => {
    this.setState({key});
  };

  render () {
    let activeComponent = '';
    if (this.state.key === 'bookings') {
      activeComponent = <Bookings />;
    } else if (this.state.key === 'clients') {
      activeComponent = <Clients />;
    } else if (this.state.key === 'vehicles') {
      activeComponent = <Vehicles />;
    } else if (this.state.key === 'employees') {
      activeComponent = <Employees />;
    }
    return (
      <Tab.Container id="body-tabs" activeKey={this.state.key} onSelect={this.handleSelect}>
        <Row className="clearfix">
          <Col sm={4}>
            <Nav bsStyle="pills" stacked>
              <NavItem key="bookings" eventKey="bookings">Bookings</NavItem>
              <NavItem key="vehicles" eventKey="vehicles">Vehicles</NavItem>
              <NavItem key="clients" eventKey="clients">Clients</NavItem>
              <NavItem key="employees" eventKey="employees">Employees</NavItem>
            </Nav>
          </Col>
          <Col sm={8}>
            <Tab.Content>
              <Tab.Pane key={this.state.key} eventKey={this.state.key}>
                {activeComponent}
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    );
  }
}
