import moxios from 'moxios';
import api from '../../services/api';
import mockStore from '../../app/tests/mockStore';
import {getEmployees, GET_EMPLOYEES} from '../redux';

const {describe, it, expect, beforeEach, afterEach} = global;


describe('getEmployees', () => {
  beforeEach(() => {
    moxios.install(api.instance);
  });

  afterEach(() => {
    moxios.uninstall(api.instance);
  });

  it('call dispatches PENDING', () => {
    const store = mockStore({});

    store.dispatch(getEmployees());
    const actions = store.getActions();
    expect(actions).toEqual([{'type': GET_EMPLOYEES.PENDING}]);
  });

  it('on success dispatches SUCCESS', () => {
    expect.assertions(3);

    const data = {'results': [{'id': 1}]};
    moxios.stubRequest('/api/employees/', {
      'status': 200,
      'response': data,
    });

    const store = mockStore({});
    const promise = store.dispatch(getEmployees());

    return promise.then((action) => {
      const actions = store.getActions();

      expect(action).toMatchObject({'response': {'ok': true, 'status': 200, 'data': data}});
      expect(actions).toEqual([
        {'type': GET_EMPLOYEES.PENDING},
        {'type': GET_EMPLOYEES.SUCCESS, 'response': action.response},
      ]);

      expect(moxios.requests.count()).toEqual(1);
    });
  });

  it('on error dispatches ERROR', () => {
    expect.assertions(3);

    moxios.stubRequest('/api/employees/', {
      'status': 501,
      'response': null,
    });

    const store = mockStore({});
    const promise = store.dispatch(getEmployees());

    return promise.catch((error) => {
      const actions = store.getActions();

      expect(error).toMatchObject({'ok': false, 'status': 501, 'problem': 'SERVER_ERROR'});
      expect(actions).toEqual([
        {'type': GET_EMPLOYEES.PENDING},
        {'type': GET_EMPLOYEES.ERROR, 'error': error},
      ]);

      expect(moxios.requests.count()).toEqual(1);
    });
  });
});
