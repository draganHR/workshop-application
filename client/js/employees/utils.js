import api from '../services/api';


export const getEmployeesChoices = () => api.listEmployees().then((response) => {
  const results = response.data.results;
  const choices = {};
  results.forEach((element) => {
    choices[element.id] = `${element.first_name} ${element.last_name}`;
  });
  return choices;
}, (error) => {
  console.log('Choices request error:', error);
});
