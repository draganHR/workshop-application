import {createReducer} from 'reduxsauce';
import {asyncActionTypes, asyncActionFactory} from '../async';
import api from '../services/api';


export const GET_EMPLOYEES = asyncActionTypes('GET_EMPLOYEES');
export const DELETE_EMPLOYEE = asyncActionTypes('DELETE_EMPLOYEE');


export const getEmployees = asyncActionFactory(GET_EMPLOYEES.BASE, (dispatch, getState) => () => {
  console.log('callback; state: ', getState());
  return api.listEmployees();
}, {});


export const deleteEmployee = asyncActionFactory(DELETE_EMPLOYEE.BASE, () => (id) => {
  console.log('deleteEmployee id:', id);
  return api.deleteEmployee(id);
}, {});


const initialState = {
  'isPending': false,
  'results': [],
  'count': 0,
  'previous': null,
  'next': null,
  'error': null,
};


const handlers = {
  [GET_EMPLOYEES.PENDING]: (state) => ({
    ...state,
    'isPending': true,
    'error': null,
  }),
  [GET_EMPLOYEES.SUCCESS]: (state, action) => ({
    ...state,
    'isPending': false,
    'error': null,
    'results': action.response.data.results,
    'count': action.response.data.count,
    'previous': action.response.data.previous,
    'next': action.response.data.next,
  }),
  [GET_EMPLOYEES.ERROR]: (state) => ({
    ...state,
    'isPending': false,
    'error': 'YES',
  }),
};


const reducer = createReducer(initialState, handlers);
export default reducer;
