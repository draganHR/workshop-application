import React from 'react';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import ValidationError from '../../forms/components/ValidationError';
import Widget from '../../forms/components/Widget';


const EmployeeForm = (props) => {
  console.log('EmployeeForm props', props);
  const {data, errors, onSubmit, onChange, onCancel} = props;
  return (
    <Form horizontal onSubmit={onSubmit}>
      <ValidationError>{errors['']}</ValidationError>
      <Widget
        name="first_name"
        label="First name"
        error={errors.first_name}
        data={data.first_name}
        onChange={onChange}
      />
      <Widget
        name="last_name"
        label="Last name"
        error={errors.last_name}
        data={data.last_name}
        onChange={onChange}
      />
      <Widget
        name="notes"
        label="Notes"
        componentClass="textarea"
        rows="6"
        error={errors.notes}
        data={data.notes}
        onChange={onChange}
      />
      <FormGroup>
        <Col smOffset={3} sm={9}>
          <ButtonToolbar>
            <Button onClick={onCancel}>Cancel</Button>
            <Button type="submit" bsStyle="primary">
              <Glyphicon glyph="save" />{' '}
              Submit
            </Button>
          </ButtonToolbar>
        </Col>
      </FormGroup>
    </Form>
  );
};

EmployeeForm.propTypes = {
  'data': PropTypes.shape({
    'first_name': PropTypes.string,
    'last_name': PropTypes.string,
    'notes': PropTypes.string,
  }).isRequired,
  'errors': PropTypes.shape({}),
  'onSubmit': PropTypes.func.isRequired,
  'onCancel': PropTypes.func.isRequired,
  'onChange': PropTypes.func.isRequired
};

export default EmployeeForm;
