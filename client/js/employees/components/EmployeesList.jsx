/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import ListGroup from 'react-bootstrap/lib/ListGroup';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';


export default class EmployeesList extends React.Component {
  render () {
    const {results} = this.props;

    return (
      <section>
        <ListGroup>
          {results.map((item) => (
            <ListGroupItem
              key={item.id}
              header={`#${item.id}`}
            >
              <span className="btn-group">
                <Button onClick={() => this.props.onEdit(item)} title="Edit"><Glyphicon glyph="edit" /></Button>
                <Button onClick={() => this.props.onDelete(item)} title="Delete"><Glyphicon glyph="trash" /></Button>
              </span>
              {item.first_name} {item.last_name}
            </ListGroupItem>
          ))}
        </ListGroup>
      </section>
    );
  }
}


EmployeesList.propTypes = {
  'results': PropTypes.arrayOf(PropTypes.shape()).isRequired,
  'onEdit': PropTypes.func,
  'onDelete': PropTypes.func,
};


EmployeesList.defaultProps = {
  'onEdit': () => {},
  'onDelete': () => {},
};
