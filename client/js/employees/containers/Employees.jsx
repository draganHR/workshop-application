import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import EmployeesList from '../components/EmployeesList';
import * as actions from '../redux';
import EmployeeEditor from './EmployeeEditor';


class Employees extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      'activePage': 1,
      'showDialog': false,
    };
  }

  componentDidMount () {
    this.loadData();
  }

  loadData () {
    this.props.actions.getEmployees();
  }

  handleNew = () => {
    this.setState({
      'activeId': null,
      'showDialog': true
    });
  };

  handleEdit = (item) => {
    this.setState({
      'activeId': item.id,
      'showDialog': true
    });
  };

  handleDelete = (item) => {
    this.props.actions.deleteEmployee(item.id).then(
      () => this.props.actions.getEmployees()
    );
  };

  handleCloseDialog = () => {
    this.setState({
      'activeId': null,
      'showDialog': false
    });
    this.loadData();
  };

  render () {
    const {showDialog, activeId} = this.state;
    return (
      <div>
        { showDialog &&
        <EmployeeEditor
          id={activeId}
          onHide={this.handleCloseDialog}
          onClose={this.handleCloseDialog}
        />
        }
        <ButtonToolbar>
          <Button onClick={this.handleNew} bsStyle="primary">New Employee</Button>
        </ButtonToolbar>
        <EmployeesList
          results={this.props.results}
          onEdit={this.handleEdit}
          onDelete={this.handleDelete}
        />
      </div>
    );
  }
}


Employees.propTypes = {
  'results': PropTypes.arrayOf(PropTypes.shape()).isRequired,
  'actions': PropTypes.shape().isRequired,
};


const mapState = (state) => ({
  'results': state.employees.results,
});


const mapDispatch = (dispatch) => ({
  'actions': bindActionCreators({
    'getEmployees': actions.getEmployees,
    'deleteEmployee': actions.deleteEmployee,
  }, dispatch)
});

export default connect(mapState, mapDispatch)(Employees);
