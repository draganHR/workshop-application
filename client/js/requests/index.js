/**
 * axios adapter based on apisauce
 *
 * License: MIT
 *
 * https://github.com/axios/axios
 * https://github.com/infinitered/apisauce
 *
 */
/* eslint-disable no-shadow */
import axios, {isCancel} from 'axios';

const isWithin = (min, max) => ((value) => min <= value && value <= max);

const defaults = {
  'timeout': 0,
  'headers': {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
};


export const in200s = isWithin(200, 299);
export const in400s = isWithin(400, 499);
export const in500s = isWithin(500, 599);

export const CLIENT_ERROR = 'CLIENT_ERROR';
export const SERVER_ERROR = 'SERVER_ERROR';
export const TIMEOUT_ERROR = 'TIMEOUT_ERROR';
export const CONNECTION_ERROR = 'CONNECTION_ERROR';
export const NETWORK_ERROR = 'NETWORK_ERROR';
export const UNKNOWN_ERROR = 'UNKNOWN_ERROR';
export const CANCEL_ERROR = 'CANCEL_ERROR';


function RequestException (result) {
  this.error = result.error;
  this.status = result.status;
  this.problem = result.problem;
  this.ok = result.ok;
  this.headers = result.headers;
  this.config = result.config;
  this.data = result.data;
  this.request = result.request;

  this.toString = () => (result.error === null ? '' : result.error.toString());
}


const getProblemFromStatus = (status) => {
  if (in200s(status)) {
    return null;
  } else if (in400s(status)) {
    return CLIENT_ERROR;
  } else if (in500s(status)) {
    return SERVER_ERROR;
  } else {
    return UNKNOWN_ERROR;
  }
};


const getProblemFromError = (error) => {
  // first check if the error message is Network Error (set by axios at 0.12) on platforms other than NodeJS.
  if (error.message === 'Network Error') {
    return NETWORK_ERROR;
  } else if (isCancel(error)) {
    return CANCEL_ERROR;
  } else if ((error.code === undefined || error.code === null) && error.response) {
    return getProblemFromStatus(error.response.status);
  } else {
    return UNKNOWN_ERROR;
  }
};


function handleResponse (value) {
  const isError = value instanceof Error || isCancel(value);
  const response = isError ? value.response : value;
  const error = isError ? value : null;
  const status = (response && response.status) || null;
  const problem = isError ? getProblemFromError(value) : getProblemFromStatus(status);
  const ok = in200s(status);
  const config = value.config || null;
  const headers = (response && response.headers) || null;
  const data = (response && response.data) || null;
  const axiosRequest = (response && response.request) || null;
  return {
    error,
    status,
    problem,
    ok,
    headers,
    config,
    data,
    'request': axiosRequest,
  };
}


function handleRequest (promise) {
  return promise.then(handleResponse, (error) => {
    throw new RequestException(handleResponse(error));
  });
}


//export default {request, get, 'delete': delete_func, head, options, post, put, patch};

const create = function create (config) {
  const combinedConfig = Object.assign({}, defaults, config);
  const headers = combinedConfig.headers;

  const instance = axios.create(combinedConfig);

  /**
   * Set header.
   */
  const setHeader = function setHeader (name, value) {
    headers[name] = value;
    return instance;
  };

  /**
   * Remove header.
   */
  const deleteHeader = function deleteHeader (name) {
    delete headers[name];
    return instance;
  };

  /**
   * Sets a new base URL.
   */
  const setBaseURL = function setBaseURL (newURL) {
    instance.defaults.baseURL = newURL;
    return instance;
  };
  /**
   * Gets the current base URL used by axios.
   */
  const getBaseURL = function getBaseURL () {
    return instance.defaults.baseURL;
  };

  function request (config) {
    return handleRequest(instance.request(config));
  }


  function get (url, config) {
    return handleRequest(instance.get(url, config));
  }


  function delete_func (url, config) {
    return handleRequest(instance.delete(url, config));
  }


  function head (url, config) {
    return handleRequest(instance.head(url, config));
  }


  function options (url, config) {
    return handleRequest(instance.options(url, config));
  }


  function post (url, data, config) {
    return handleRequest(instance.post(url, data, config));
  }


  function put (url, data, config) {
    return handleRequest(instance.put(url, data, config));
  }


  function patch (url, data, config) {
    return handleRequest(instance.patch(url, data, config));
  }

  return {
    instance,
    setHeader,
    deleteHeader,
    request,
    get,
    'delete': delete_func,
    head,
    options,
    post,
    put,
    patch
  };
};

export default create;
