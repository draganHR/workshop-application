import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import configureStore from 'redux-mock-store';

const middlewares = [
  promiseMiddleware(),
  thunkMiddleware, // lets us dispatch() functions
];

const mockStore = configureStore(middlewares);
export default mockStore;
