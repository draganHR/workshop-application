import store from '../store';

const {describe, it, expect} = global;


describe('A store', () => {
  it('is configured', () => {
    expect(store).toHaveProperty('subscribe');
    expect(store).toHaveProperty('dispatch');
    expect(store).toHaveProperty('getState');
  });
});
