import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import profileReducer from '../profile/redux';
import bookingsReducer from '../bookings/redux';
import clientsReducer from '../clients/redux';
import vehiclesReducer from '../vehicles/redux';
import employeesReducer from '../employees/redux';

const reducer = combineReducers({
  'profile': profileReducer,
  'bookings': bookingsReducer,
  'clients': clientsReducer,
  'vehicles': vehiclesReducer,
  'employees': employeesReducer,
});

const middlewares = [
  promiseMiddleware(),
  thunkMiddleware, // lets us dispatch() functions
];

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  composeEnhancers(applyMiddleware(...middlewares)),
);
export default store;
