module.exports = {
  'verbose': true,
  'roots': [
    'client',
  ],
  'setupFiles': [
    'babel-polyfill',
  ],
  'transform': {
    '.*': '<rootDir>/node_modules/babel-jest',
  },
  'transformIgnorePatterns': [
    '/node_modules/babel-runtime/',
    '/node_modules/regenerator-runtime/',
    '/node_modules/jest-runtime/'
  ],
  'unmockedModulePathPatterns': [
    '<rootDir>/node_modules/react',
    '<rootDir>/node_modules/react-dom',
    '<rootDir>/node_modules/react-addons-test-utils',
    '<rootDir>/node_modules/fbjs'
  ],
};
