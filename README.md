Bootstrap (as root)

    sh bootstrap

Installing node libraries and build bundles

    make node-setup

Building sprites

    yarn run sprites

Prepare server:

    python manage.py migrate

Running django server:

    python manage.py runserver 0.0.0.0:8080

## Issues

 - No auto reload or hot replacement
 - Not watching all Django static file directories

## Info

Icons: http://www.famfamfam.com/lab/icons/silk/

## TODO

- Fix authentication in pg_hba.conf & pg_ident.conf
