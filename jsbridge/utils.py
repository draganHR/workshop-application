import os
import re

from django.urls import get_resolver, get_script_prefix
from django.conf import settings
from django.template import loader
from django.utils.safestring import mark_safe
from django.utils.six import StringIO, string_types

try:
    import simplejson as json
except ImportError:
    import json


JSBRIDGE_PATH = getattr(settings, 'JSBRIDGE_PATH', 'jsbridge.js')
JSBRIDGE_STATIC_PATH = getattr(settings, 'JSBRIDGE_STATIC_PATH', None)  # optional
JSROUTES_MODULE_NAME = getattr(settings, 'JSROUTES_VARIABLE_NAME', 'jsbridge')
JSBRIDGE_ROUTES = getattr(settings, 'JSBRIDGE_ROUTES', ())
JSBRIDGE_ROUTES_ALLOW_MISSING = getattr(settings, 'JSBRIDGE_ROUTES_ALLOW_MISSING', False)


jsbridge_patterns = {entry: re.compile(entry) for entry in JSBRIDGE_ROUTES}


def check_url_name(name):
    result = set()
    if not isinstance(name, string_types):
        return result
    for original, pattern in jsbridge_patterns.items():
        if pattern.match(name):
            result.add(original)
    return result


def get_routes():
    url_patterns = list(get_resolver(None).reverse_dict.items())
    routes = []
    all_matched_routes = set()

    for url_name, url_pattern in url_patterns:
        matched_routes = check_url_name(url_name)
        if matched_routes:
            routes.append((url_name, url_pattern[0][0]))
            all_matched_routes |= matched_routes

    if not JSBRIDGE_ROUTES_ALLOW_MISSING:
        missing_routes = set(JSBRIDGE_ROUTES) - all_matched_routes
        if missing_routes:
            raise Exception('Routes not found: %s' % str(missing_routes))

    return routes


def render():
    exports = getattr(settings, 'JSBRIDGE_EXPORTS', {})

    context = {'module_name': JSROUTES_MODULE_NAME,
               'exports': {name: mark_safe(json.dumps(value))
                           for name, value in exports.items()},
               'routes': get_routes(),
               'route_prefix': get_script_prefix(),
               }

    t = loader.get_template('jsbridge/index.js')
    return t.render(context)


def save_to_storage(storage):
    path = JSBRIDGE_PATH
    content = StringIO(render())
    storage.delete(path)
    storage.save(path, content)


def save_to_static():
    if JSBRIDGE_STATIC_PATH:
        f = open(os.path.join(JSBRIDGE_STATIC_PATH, JSBRIDGE_PATH), 'w')
        f.write(render())
        f.close()
