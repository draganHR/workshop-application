import sys

from django.core.management.base import BaseCommand
from django.contrib.staticfiles.storage import staticfiles_storage

from jsbridge.utils import render, save_to_static, save_to_storage


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--storage', action='store_true', dest='storage', default=False,
                            help='Rebuild frontend code')
        parser.add_argument('--static', action='store_true', dest='static', default=False,
                            help='Rebuild frontend code')
        super(Command, self).add_arguments(parser)

    def handle(self, cmd=None, *args, **options):
        if not options['storage'] and not options['static']:
            sys.stdout.write(render())
        if options['storage']:
            sys.stdout.write("Saving jsbridge to storage...\n")
            save_to_storage(staticfiles_storage)
        if options['static']:
            sys.stdout.write("Saving jsbridge to static...\n")
            save_to_static()
