/**
 * {{module_name}} - Django javascript integration utility
 */
(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.{{ module_name }} = mod.exports;
  }
})(this, function (exports) {

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  {% for name, value in exports.items %}exports.{{ name }} = {{ value }};
  {% endfor %}
  /**
   * Generate url for given route name and arguments
   *
   * Examples:
   *   route('route_name')
   *   route('route_name', ['foo', 'bar', 'baz'])
   *   route('route_name', {key1: 'foo', key2: 'bar'})
   *   route('route_name', ['foo'], {querystring_key1: 'bar'})
   *
   * Route arguments:
   *   {%  for name, route in routes %}
   *   {{name}}:
   *     {% if route.1 %}{% for argname in route.1 %}{{argname}}{% if not forloop.last %},{% endif %}{% endfor %}{% else %}(none){% endif %}{% endfor %}
   *
   * @param {object} args - Object or array containing route arguments (hash_value)
   * @param {object=} qsargs - Object containing query string values
   */
  var route = function (name, args, qsargs) {
    var params = route.routes[name];
    var path = route.reverse(params.pattern, params.keys, args);
    var qs = route.serialize(qsargs);
    return route.prefix + path + (qs ? '?' + qs : '');
  };

  route.routes = {{%  for name, route in routes %}
    '{{name}}': {
      'pattern': '{{route.0}}',
      'keys': [{% for argname in route.1 %}"{{argname}}"{% if not forloop.last %},{% endif %}{% endfor %}]
    },{% endfor %}
  };

  route.prefix = '{{route_prefix|escapejs}}';

  route.reverse = function (route, keys, args) {
    args = args || {};
    var i;
    var value;
    var isArray = (args instanceof Array);
    for (i = 0; i < keys.length; i++) {
      value = isArray ? (i < args.length ? args[i] : '') : (args[keys[i]] !== undefined ? args[keys[i]] : '');
      route = route.replace(new RegExp("%\\(" + keys[i] + "\\)s", 'g'), value);
    }
    return route;
  };

  route.serialize = function (obj) {
    var p;
    var str = [];
    if (typeof obj === 'string' || obj instanceof String) {
      return obj;
    }
    for (p in obj) {
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    }
    return str.join('&');
  };

  exports.route = route;
});
