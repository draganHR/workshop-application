#!/usr/bin/make -f

export NODE_MIRROR := ${HOME}/node-libs.tmp


clean_pyc:
	find . -regex ".+\.py[co]" -delete
	find server -depth -type d -empty -delete

node-clean:
	rm -rf ${NODE_MIRROR}
	rm -rf node_modules

node-clean-all:
	rm -rf ~/.yarn-cache/
	$(MAKE) node-clean

node-install:
	mkdir -p node_modules
	mkdir -p ${NODE_MIRROR}
	cp package.json ${NODE_MIRROR}/package.json
	cp .yarnrc ${NODE_MIRROR}/.yarnrc
	cp yarn.lock ${NODE_MIRROR}/yarn.lock
	cd ${NODE_MIRROR} && yarn install --production=false
	find ${NODE_MIRROR} -type l -delete
	rsync -a --delete ${NODE_MIRROR}/node_modules/ node_modules
	cp ${NODE_MIRROR}/yarn.lock yarn.lock
	yarn run bin

node-build-production:
	NODE_ENV=production ./node_modules/.bin/webpack -d

node-setup:
	$(MAKE) node-install
	yarn run build

dumpdata:
	python manage.py dumpdata --indent 2 -o install/fixtures.json workshop
