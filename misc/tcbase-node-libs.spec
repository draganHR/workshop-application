Name:           tcbase-node-libs
Version:        {{ VERSION }}
Release:        {{ RELEASE }}
Summary:        ReversingLabs tcbase npm packages

License:        N/A

URL:            http://reversinglabs.com
Source0:        %{name}-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-buildroot
Prefix:         /opt
%define install_dir %{prefix}/%{name}
%define  debug_package %{nil}

BuildArch:     noarch
Requires: nodejs >= 0.12.7

%description
ReversingLabs tcbase npm packages

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT%{install_dir}
install -m 0755 -d $RPM_BUILD_ROOT%{install_dir}
cp -a * $RPM_BUILD_ROOT%{install_dir}
#sh $RPM_BUILD_ROOT%{install_dir}/bin.sh

%clean
rm -rf $RPM_BUILD_ROOT

%post

%files
%defattr(-,root,root,-)
%doc
%dir %{install_dir}
%{install_dir}/*
