import logging

import six
from django.core.exceptions import PermissionDenied
from django.http import Http404
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.views import exception_handler as default_exception_handler
from rest_framework.authtoken.views import ObtainAuthToken as DefaultObtainAuthToken
from rest_framework.decorators import api_view


logger = logging.getLogger(__name__)


@api_view(['GET', 'POST'])
def not_found_view(request):
    raise exceptions.NotFound()


def exception_handler(exc, context):

    if (isinstance(exc, exceptions.NotAuthenticated) and
            exc.status_code != exceptions.NotAuthenticated.status_code):
        # DRF, depending on authentication classes, will change
        # status code 401 to 403 for NotAuthenticated exception
        exc.status_code = exceptions.NotAuthenticated.status_code

    # get standard rest_framework error response
    response = default_exception_handler(exc, context)

    # Unhandled exceptions will raise a 500 error.
    if response is None:
        return response

    if isinstance(response.data, dict) and isinstance(response.data.get('detail'), six.string_types):
        message = response.data['detail']
        detail = {}
    elif isinstance(exc, exceptions.ValidationError):
        message = "Validation error."
        detail = {"fields": response.data}  # Validation errors
    elif isinstance(exc, exceptions.APIException):
        message = "Request error."
        detail = response.data['detail']
    elif isinstance(exc, Http404):
        message = "Not found."
        detail = response.data['detail']
    elif isinstance(exc, PermissionDenied):
        message = "Permission denied."
        detail = response.data['detail']
    else:
        logger.exception("Unexpected exception")
        message = "Unexpected error."
        detail = response.data['detail']

    return Response({
        "status": response.status_code,
        "message": message,
        "detail": detail,
    }, status=response.status_code, headers=getattr(response, '_headers'))


class ObtainAuthToken(DefaultObtainAuthToken):
    def get_serializer_class(self):
        # swagger compatibility
        return self.serializer_class

    def post(self, request):
        """
        Obtain Auth Token.

        Method does not require authentication.

        ### Example request

            curl -X POST --form "username=user123" --form "password=pass123" http://192.168.1.101/api-token-auth/

        ### Example request using Python and requests library

            import requests
            requests.post('http://192.168.1.101/api-token-auth/',
                          data={'username': 'user123', 'password': 'pass123'})

        ### Example response body

            {
              "token": "988881adc9fc3655077dc2d4d757d480b5ea0e11"
            }

        ---
        responseMessages:
            - code: 200
              message: OK
            - code: 400
              message: Unable to log in with provided credentials.
        """
        return super(ObtainAuthToken, self).post(request)
