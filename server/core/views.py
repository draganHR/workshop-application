from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView


index_view = login_required(TemplateView.as_view(template_name='core/index.html'))
