from rest_framework import serializers
from django.contrib.auth import get_user_model


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email')

    username = serializers.CharField(
        label='First Name',
        required=False,
        allow_blank=True,
        default=''
    )

    first_name = serializers.CharField(
        label='First Name',
        required=False,
        allow_blank=True,
        default=''
    )

    last_name = serializers.CharField(
        label='Last Name',
        required=False,
        allow_blank=True,
        default=''
    )

    email = serializers.EmailField(
        label='Email',
        allow_blank=False,
    )
