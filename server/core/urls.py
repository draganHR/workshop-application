from django.conf.urls import include, url

from .views import index_view
from .api import ProfileView


urlpatterns = (
    url(r'^$', index_view, name='index'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^api/profile/$', ProfileView.as_view(), name='api-user-profile'),
)
