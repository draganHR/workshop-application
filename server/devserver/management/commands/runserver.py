import subprocess

from django.core.management.commands.runserver import BaseRunserverCommand
from django.conf import settings
from jsbridge.utils import save_to_static


class Command(BaseRunserverCommand):

    def add_arguments(self, parser):
        parser.add_argument('--build', action='store_true', dest='build', default=False,
                            help='Rebuild frontend code')
        super(Command, self).add_arguments(parser)

    def inner_run(self, *args, **options):
        self.run_jsbridge()
        self.run_frontend(options['build'])
        super(Command, self).inner_run(*args, **options)

    def run_frontend(self, build):
        base_dir = settings.BASE_DIR

        if build:
            yarn_command = "/usr/bin/yarn run start"
        else:
            yarn_command = "/usr/bin/yarn run watch"

        self.stdout.write("Starting frontend process\n")

        process = subprocess.Popen([yarn_command],
                                   shell=True,
                                   cwd=base_dir,
                                   stdin=subprocess.PIPE,
                                   stdout=self.stdout,
                                   stderr=self.stderr)
        self.stdout.write("Frontend process started on %r\n" % process.pid)

    def run_jsbridge(self):
        save_to_static()
