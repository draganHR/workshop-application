from rest_framework import viewsets, filters

from server.workshop.models import Client, Make, Model, Vehicle, Employee, Booking
from server.workshop.serializers import ClientSerializer, MakeSerializer, ModelSerializer, VehicleSerializer, \
    EmployeeSerializer, BookingSerializer


class ClientViewSet(viewsets.ModelViewSet):
    """
    A ViewSet for viewing and editing Clients.
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id',)
    ordering = ('-id',)


class MakeViewSet(viewsets.ModelViewSet):
    """
    A ViewSet for viewing and editing Makes.
    """
    queryset = Make.objects.all()
    serializer_class = MakeSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id',)
    ordering = ('-id',)


class ModelViewSet(viewsets.ModelViewSet):
    """
    A ViewSet for viewing and editing Models.
    """
    queryset = Model.objects.all()
    serializer_class = ModelSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id',)
    ordering = ('-id',)


class VehicleViewSet(viewsets.ModelViewSet):
    """
    A ViewSet for viewing and editing Vehicles.
    """
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id',)
    ordering = ('-id',)


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    A ViewSet for viewing and editing Employees.
    """
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id',)
    ordering = ('-id',)


class BookingViewSet(viewsets.ModelViewSet):
    """
    A ViewSet for viewing and editing Bookings.
    """
    queryset = Booking.objects.all()
    serializer_class = BookingSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id', 'priority', 'status')
    ordering = ('-id',)
