from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from server.workshop import api


router = DefaultRouter()
router.register(r'clients', api.ClientViewSet, base_name='client')
router.register(r'makes', api.MakeViewSet, base_name='make')
router.register(r'models', api.ModelViewSet, base_name='model')
router.register(r'vehicles', api.VehicleViewSet, base_name='vehicle')
router.register(r'employees', api.EmployeeViewSet, base_name='employee')
router.register(r'bookings', api.BookingViewSet, base_name='booking')

urlpatterns = (
    url(r'^api/', include(router.urls, namespace='api')),
)
