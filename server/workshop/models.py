from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Client(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email_address = models.CharField(max_length=255)
    phone = models.CharField(max_length=50, blank=True)
    address_line_1 = models.CharField(max_length=128, blank=True)
    address_line_2 = models.CharField(max_length=128, blank=True)
    address_line_3 = models.CharField(max_length=128, blank=True)
    city = models.CharField(max_length=128)
    postal = models.PositiveIntegerField()
    country = models.CharField(max_length=128)
    notes = models.TextField(blank=True)
    created_at = models.DateField(blank=True, auto_now_add=True)

    def __str__(self):
        return "%s %s (%s)" % (self.first_name, self.last_name, self.id)


@python_2_unicode_compatible
class Make(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Model(models.Model):
    make = models.ForeignKey(Make)
    name = models.CharField(max_length=128)

    def __str__(self):
        return "%s %s" % (self.make, self.name)


@python_2_unicode_compatible
class Vehicle(models.Model):
    client = models.ForeignKey(Client)
    model = models.ForeignKey(Model)
    year = models.PositiveIntegerField(null=True)
    license_number = models.CharField(max_length=24, blank=True)
    current_mileage = models.PositiveIntegerField(null=True)
    engine_size = models.CharField(max_length=12, blank=True)
    notes = models.TextField(blank=True)

    def __str__(self):
        return "%s (%s): %s" % (self.model, self.year, self.license_number)


@python_2_unicode_compatible
class Employee(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    notes = models.TextField(blank=True)

    def __str__(self):
        return "%s %s (%s)" % (self.first_name, self.last_name, self.id)


@python_2_unicode_compatible
class Booking(models.Model):
    vehicle = models.ForeignKey(Vehicle)
    employee = models.ForeignKey(Employee)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.SmallIntegerField(choices=(
        (0, 'New'),
        (1, 'Pending'),
        (2, 'Working'),
        (3, 'Finished'),
        (4, 'Closed'),
    ), default=0)
    billed_at = models.DateTimeField(null=True)
    priority = models.SmallIntegerField(choices=(
        (0, 'Low'),
        (1, 'Medium'),
        (2, 'High'),
    ), default=0)
    hours_spent = models.PositiveIntegerField(default=0)
    notes = models.TextField(blank=True)

    def __str__(self):
        return "%s - %s (%s)" % (self.vehicle, self.employee, self.status)
