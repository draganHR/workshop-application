from django.conf.urls import url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = [
    url(r'^', include('server.core.urls')),
    url(r'^', include('server.workshop.urls')),
]

urlpatterns += tuple(staticfiles_urlpatterns())
