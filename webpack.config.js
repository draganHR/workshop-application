const webpack = require('webpack');
const WatchFilterPlugin = require('./node_tasks/watch-filter');
const utils = require('./node_tasks/utils');
const path = require('path');

const nodePath = process.env.NODE_PATH ? process.env.NODE_PATH : path.resolve(__dirname, 'node_modules');

const sourceDir = path.resolve(__dirname, 'client');
const outputDir = path.resolve(__dirname, 'client_output');

const NODE_ENV = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const isProduction = NODE_ENV === 'production';
process.env.NODE_ENV = NODE_ENV;

const DEBUG = JSON.stringify(JSON.parse(process.env.DEBUG || 'true'));
const plugins = [];


function resolvePath (name) { return path.join(sourceDir, name); }

// Prep all entry points
const entry = {
  'shared': [
    'babel-polyfill',
    'react',
    'react-dom',
    'react-redux',
    'react-bootstrap',
    'redux',
    'reduxsauce',
    'redux-thunk',
    'type-to-reducer',
    'axios',
    resolvePath('js/setup'),
  ]
};

const bundles = utils.getBundles(path.join(sourceDir, 'js'));
Object.keys(bundles).forEach((name) => {
  entry[name] = [bundles[name]];
});

plugins.push(
  new WatchFilterPlugin()
);

// definePlugin takes raw strings and inserts them, so you can put strings of JS if you want.
plugins.push(
  new webpack.DefinePlugin({
    '__DEBUG__': DEBUG,
    'process.env': {
      'NODE_ENV': JSON.stringify(NODE_ENV),
    },
  })
);

if (isProduction) {
  plugins.push(
    new webpack.optimize.ModuleConcatenationPlugin()
  );
}

plugins.push(
  new webpack.optimize.CommonsChunkPlugin({
    'name': ['shared'],
    'filename': '[name].js',
    'minChunks': Infinity
  })
);

plugins.push(
  new webpack.LoaderOptionsPlugin({
    'minimize': true,
    'debug': false
  })
);

if (isProduction) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      'sourceMap': true,
      'compress': {
        'drop_console': false,
        'drop_debugger': true,
        'warnings': false
      }
    })
  );
}

module.exports = {
  'entry': entry,
  'devtool': isProduction ? 'source-map' : 'inline-source-map',
  'target': 'web',
  'output': {
    'path': path.join(outputDir, 'js'),
    'filename': '[name].bundle.js',
  },
  'module': {
    'rules': [
      {
        'test': /\.jsx?$/,
        'loader': 'babel-loader',
        'options': {
        },
      }
    ]
  },
  'resolve': {
    'extensions': ['.js', '.jsx', '.json'],
    'modules': [nodePath]
  },
  'watchOptions': {
    'aggregateTimeout': 300,
    'poll': 1500
  },
  'plugins': plugins
};
